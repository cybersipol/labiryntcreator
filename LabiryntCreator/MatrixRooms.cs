﻿using LabiryntCreator.Enums;
using LabiryntCreator.Interfaces;
using System;
using System.Collections;

namespace LabiryntCreator
{
    public class MatrixRooms : IMatrixWithNeighbors<IRoom>
    {
        public MatrixRooms(ushort numRows, ushort numColumns, IRoomFactory roomFactory = null)
        {
            CountColumns = numColumns;
            CountRows = numRows;
            RoomFactory = roomFactory;
            FillAllElements();
        }

        private ushort enumRow = 0, enumCol = 0;

        public ushort CurrentRow => enumRow;
        public ushort CurrentColumn => enumCol;

        private IRoom[][] Elements { get; set; } = null;
        public ushort CountColumns { get; private set; } = 0;
        public ushort CountRows { get; private set; } = 0;
        public uint Count => getCountElements();

        public IRoomFactory RoomFactory { get; set; } = null;
        public IRoom Current => GetElementOrNull(enumRow, enumCol);
        object IEnumerator.Current => GetElementOrNull(enumRow, enumCol);
        public IRoom this[ushort row, ushort col] { get => GetElement(row, col); set => SetElement(row, col, value); }


        public void Dispose() { }

        public bool MoveNext()
        {
            enumCol++;
            if (enumCol>=CountColumns) { enumCol = 0; enumRow++; }
            return (enumRow < CountRows);                
        }

        public void Reset()
        {
            enumRow = 0;
            enumCol = 0;
        }

        public IRoom GetElement(ushort row, ushort column)
        {
            CheckRowAndColumnIndex(row, column);
            return Elements[row][column];
        }

        public void SetElement(ushort row, ushort column, IRoom element)
        {
            CheckRowAndColumnIndex(row, column);
            Elements[row][column] = element;
        }



        public IRoom GetElementFromDirection(IRoom fromElem, Direction direction)
        {
            DirectionDelta delta = DirectionDelta.GetDelta(direction);
            if (delta != null) return GetElementFrom(fromElem, delta.DeltaRows, delta.DeltaColumns);
            return null;
        }

        public void FillAllElements()
        {
            Elements = null;
            if (CountRows <= 0 || CountColumns <= 0) return;

            Elements = new IRoom[CountRows][];
            int row = 0, col = 0;
            for (row = 0; row < CountRows; row++)
            {
                Elements[row] = new IRoom[CountColumns];
                for (col = 0; col < CountColumns; col++)
                    Elements[row][col] = RoomFactory?.CreateRoom();
            }
        }

        public IMatrixPosition SearchElement(IRoom element) => SearchRoom(element);


        #region Help functions


        private void CheckRowAndColumnIndex(ushort row, ushort column)
        {
            if (row >= CountRows) throw new ArgumentOutOfRangeException(nameof(row));
            if (column >= CountColumns) throw new ArgumentOutOfRangeException(nameof(column));
        }

        private IRoom GetElementOrNull(ushort row, ushort col)
        {
            if (row >= CountRows) return null;
            if (col >= CountColumns) return null;

            return Elements[row][col];
        }

        private uint getCountElements()
        {
            if (Elements == null) return 0;

            uint count = 0;
            int row, col;
            for (row = 0; row < CountRows; row++)
                for (col = 0; col < CountColumns; col++)
                    if (Elements[row][col] != null) count++;

            return count;
        }

        private RoomPosition SearchRoom(IRoom room)
        {
            ushort row, col;
            for (row = 0; row < CountRows; row++)
                for (col = 0; col < CountColumns; col++)
                    if (Elements[row][col] == room) return new RoomPosition(row, col);

            return null;
        }

        private IRoom GetElementFrom(IRoom fromElem, int deltaRow, int deltaCol)
        {
            RoomPosition pos = SearchRoom(fromElem);
            return pos != null ? GetElementOrNull((ushort)(pos.Row + deltaRow), (ushort)(pos.Column + deltaCol)) : null;
        }

        #endregion


        #region Help classes
        private class RoomPosition: IMatrixPosition
        {
            public RoomPosition(ushort row, ushort col)
            {
                Row = row;
                Column = col;
            }

            public ushort Row { get; private set; }
            public ushort Column { get; private set; }
        }

        #endregion
    }
}
