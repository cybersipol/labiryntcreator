﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabiryntCreator.Interfaces
{
    public interface IMatrixPosition
    {
        ushort Row { get; }
        ushort Column { get; }
    }
}
