﻿using LabiryntCreator.Enums;

namespace LabiryntCreator.Interfaces
{
    public interface IRoom
    {
        bool this[Direction wall] { get; set; }
    }
}