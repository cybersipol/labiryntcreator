﻿using LabiryntCreator.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabiryntCreator.Interfaces
{
    public interface IMatrixWithNeighbors<T>: IMatrix<T> where T: class
    {
        T GetElementFromDirection(T fromElem, Direction direction);
    }
}
