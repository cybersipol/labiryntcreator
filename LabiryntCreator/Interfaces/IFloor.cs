﻿namespace LabiryntCreator.Interfaces
{
    public interface IFloor<T>
    {
        IMatrix<IRoom> FloorBoxes { get; }  
        ushort Width { get; }
        ushort Height { get; }
        T BoxImage { get; }

        void GenerateFloorBoxes();
    }
}