﻿using LabiryntCreator.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabiryntCreator.Interfaces
{
    public interface IWalker
    {
        IRoom CurrentRoom { get; }
        void Go(Direction direction);
    }
}
