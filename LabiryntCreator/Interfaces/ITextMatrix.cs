﻿namespace LabiryntCreator.Interfaces
{
    public interface ITextMatrix
    {
        char GetCharAtPosition(int row, int col);
        void SetCharAtPosition(int row, int col, char val);
        void RemoveCharAtPosition(int row, int col);
        void ClearAllChars();
    }
}
