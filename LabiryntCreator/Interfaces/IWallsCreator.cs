﻿namespace LabiryntCreator.Interfaces
{
    public interface IWallsCreator
    {
        void BuildWalls();
    }
}