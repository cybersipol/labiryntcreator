﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabiryntCreator.Interfaces
{
    public interface IRoomFactory
    {
        IRoom CreateRoom();
    }
}
