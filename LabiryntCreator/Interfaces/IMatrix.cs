﻿using System.Collections.Generic;

namespace LabiryntCreator.Interfaces
{
    public interface IMatrix<T>: IEnumerator<T> where T: class
    {
        uint Count { get; }
        ushort CountColumns { get; }
        ushort CountRows { get; }        

        T GetElement(ushort row, ushort column);
        void SetElement(ushort row, ushort column, T element);

        ushort CurrentColumn { get; }
        ushort CurrentRow { get; }


        T this[ushort row, ushort col] { get; set; }

        void FillAllElements();

        IMatrixPosition SearchElement(T element);
    }
}