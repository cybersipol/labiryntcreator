﻿using LabiryntCreator.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabiryntCreator
{
    class Program
    {
        static void Main(string[] args)
        {
            IRoomFactory roomFactory = new RoomWithNeighboringWallsFactory(new RoomFactory());

            IMatrixWithNeighbors<IRoom> matrixRooms = new MatrixRooms(4, 6, roomFactory);
            ((RoomWithNeighboringWallsFactory)roomFactory).RoomsMatrix = matrixRooms;
            ((MatrixRooms)matrixRooms).RoomFactory = roomFactory;

            matrixRooms.FillAllElements();



            IWallsCreator wallsCreator = new WallsCreator(matrixRooms);
            wallsCreator.BuildWalls();

            ITextPreview labiryntTextPreview = new LabiryntTextPreview(matrixRooms);

            Console.WriteLine(labiryntTextPreview.ToString());

            Console.ReadKey();

        }
    }
}
