﻿using System.Collections.Generic;
using System;

namespace LabiryntCreator.Enums
{
    public class DirectionOpposed
    {
        private static Dictionary<Direction, Direction> opposed = new Dictionary<Direction, Direction>()
        {
            { Direction.Forward, Direction.Backwards },
            { Direction.Backwards, Direction.Forward },
            { Direction.Left, Direction.Right },
            { Direction.Right, Direction.Left }
        };

        public static Direction GetOpposed(Direction wallDirection)
        {
            if (opposed.ContainsKey(wallDirection))
                return opposed[wallDirection];

            throw new ArgumentException(String.Format("No opposed direction for '{0}'", wallDirection.ToString()));
        }
    }
}
