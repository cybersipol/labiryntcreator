﻿namespace LabiryntCreator.Enums
{
    public class DirectionDelta
    {
        public DirectionDelta(int deltaRows, int deltaColumns)
        {
            DeltaRows = deltaRows;
            DeltaColumns = deltaColumns;
        }

        public int DeltaRows { get; private set; }
        public int DeltaColumns { get; private set; }

        public static DirectionDelta GetDelta(Direction wallDir)
        {
            switch (wallDir)
            {
                case Direction.Forward:
                    return new DirectionDelta(-1, 0);
                case Direction.Backwards:
                    return new DirectionDelta(1, 0);
                case Direction.Left:
                    return new DirectionDelta(0, -1);
                case Direction.Right:
                    return new DirectionDelta(0, 1);
            }

            return null;
        }
    }
}
