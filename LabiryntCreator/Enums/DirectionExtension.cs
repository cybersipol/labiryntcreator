﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabiryntCreator.Enums
{
    public static class DirectionExtension
    {
        public static int CountAll(this Direction dir)
        {
            return ((Direction[])Enum.GetValues(typeof(Direction))).Length;
        }

        public static IEnumerator<Direction> GetEnumeratorForAll(this Direction dir)
        {
            return ((Direction[])Enum.GetValues(typeof(Direction))).AsEnumerable<Direction>().GetEnumerator();
        }

        public static Array GetArrayForAll(this Direction dir)
        {
            return ((Direction[])Enum.GetValues(typeof(Direction)));
        }

        public static Array GetArrayOther(this Direction dir)
        {
            List<Direction> lst = new List<Direction>();
            lst.AddRange( dir.GetArrayForAll().OfType<Direction>() );
            lst.Remove(dir);
            return lst.ToArray();
        }

        public static Direction Opposed(this Direction direction)
        {
            return DirectionOpposed.GetOpposed(direction);
        }
    }
}
