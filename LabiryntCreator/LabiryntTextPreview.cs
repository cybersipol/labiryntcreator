﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LabiryntCreator.Interfaces;
using LabiryntCreator.Enums;

namespace LabiryntCreator
{
    public class LabiryntTextPreview : ITextPreview, ITextMatrix
    {
        public readonly uint COUNT_TEXT_LINES_PER_ONEROOM = 3;
        public readonly uint COUNT_TEXT_CHAR_COLUMN_PER_ONEROOM = 3;

        public LabiryntTextPreview(IMatrix<IRoom> roomsMatrix)
        {
            RoomsMatrix = roomsMatrix ?? throw new ArgumentNullException(nameof(roomsMatrix));
            InitOutText();
        }

        public IMatrix<IRoom> RoomsMatrix { get; private set; } = null;

        private List<RoomPositionChar> roomPositionChars = new List<RoomPositionChar>();

        private char[][] outText = null;
        

        public char EMPTY_CHAR { get; set; } = ' ';
        public char NO_ROOM_CHAR { get; set; } = '.';
        public char ROOM_CHAR { get; set; } = ' ';
        public char ROOM_LEFT_WALL_CHAR { get; set; } = '|';
        public char ROOM_RIGHT_WALL_CHAR { get; set; } = '|';
        public char ROOM_UP_WALL_CHAR { get; set; } = '-';
        public char ROOM_DOWN_WALL_CHAR { get; set; } = '-';
        public char ROOM_CROSS_CHAR { get; set; } = '+';


        public String TextPreview()
        {
            StringBuilder sb = new StringBuilder("");

            FillOutText();

            foreach (char[] charLine in outText)
            {
                String line = new string(charLine);
                sb.AppendLine(line);
            }      

            return sb.ToString();
        }

        public override string ToString()
        {
            return TextPreview();
        }

        public char GetCharAtPosition(int row, int col)
        {
            RoomPositionChar newRoomPosChar = new RoomPositionChar((ushort)row, (ushort)col);
            int k = roomPositionChars.IndexOf(newRoomPosChar);
            return (k >= 0 ? roomPositionChars[k].Char : ROOM_CHAR);
        }

        public void SetCharAtPosition(int row, int col, char val)
        {
            RoomPositionChar newRoomPosChar = new RoomPositionChar((ushort)row, (ushort)col, val);

            int k = roomPositionChars.IndexOf(newRoomPosChar);
            if (k < 0) roomPositionChars.Add(newRoomPosChar);
            else roomPositionChars[k].Char = val;

        }

        public void RemoveCharAtPosition(int row, int col)
        {
            RoomPositionChar newRoomPosChar = new RoomPositionChar((ushort)row, (ushort)col);

            int k = roomPositionChars.IndexOf(newRoomPosChar);
            if (k >= 0) roomPositionChars.Remove(newRoomPosChar);
        }

        public void ClearAllChars()
        {
            roomPositionChars.Clear();
        }


        #region Help functions

        private void InitOutText()
        {
            int row, col;

            outText = null;

            outText = new char[RoomsMatrix.CountRows * COUNT_TEXT_LINES_PER_ONEROOM][];

            for (row = 0; row < RoomsMatrix.CountRows * COUNT_TEXT_LINES_PER_ONEROOM; row++)
            {
                outText[row] = new char[RoomsMatrix.CountColumns * COUNT_TEXT_CHAR_COLUMN_PER_ONEROOM];
                for (col = 0; col < RoomsMatrix.CountColumns * COUNT_TEXT_CHAR_COLUMN_PER_ONEROOM; col++)
                    outText[row][col] = EMPTY_CHAR;
            }
        }

        

        private void FillOutText()
        {
            ushort row, col;

            for(row=0;row<RoomsMatrix.CountRows;row++)
            {
                for (col=0;col<RoomsMatrix.CountColumns;col++)
                {
                    SetOutTextForRoom(row, col);
                }
            }

        }

        private uint MatrixRowToCharUpLine(ushort row)
        {
            return row * COUNT_TEXT_LINES_PER_ONEROOM;
        }

        private uint MatrixRowToCharRoomLine(ushort row)
        {
            return MatrixRowToCharUpLine(row) + 1;
        }

        private uint MatrixRowToCharDownLine(ushort row)
        {
            return row * COUNT_TEXT_LINES_PER_ONEROOM + 2;
        }

        private uint MatrixColToCharLeftCol(ushort col)
        {
            return col * COUNT_TEXT_CHAR_COLUMN_PER_ONEROOM;
        }

        private uint MatrixColToCharRoomCol(ushort col)
        {
            return col * COUNT_TEXT_CHAR_COLUMN_PER_ONEROOM + 1;
        }

        private uint MatrixColToCharRightCol(ushort col)
        {
            return col * COUNT_TEXT_CHAR_COLUMN_PER_ONEROOM + 2;
        }


        private void SetOutTextForRoom(ushort row, ushort col)
        {

            uint upCharRow = MatrixRowToCharUpLine(row);
            uint roomCharRow = MatrixRowToCharRoomLine(row);
            uint downCharRow = MatrixRowToCharDownLine(row);

            uint leftCharCol = MatrixColToCharLeftCol(col);
            uint roomCharCol = MatrixColToCharRoomCol(col);
            uint rightCharCol = MatrixColToCharRightCol(col);

            IRoom room = RoomsMatrix.GetElement(row, col);

            // corners chars
            outText[upCharRow][leftCharCol] = ROOM_CROSS_CHAR;
            outText[downCharRow][leftCharCol] = ROOM_CROSS_CHAR;
            outText[upCharRow][rightCharCol] = ROOM_CROSS_CHAR;
            outText[downCharRow][rightCharCol] = ROOM_CROSS_CHAR;

            //room char
            outText[roomCharRow][roomCharCol] = (room != null ? GetCharAtPosition(row, col) : NO_ROOM_CHAR);

            if (room == null) return;

            // up wall
            outText[upCharRow][roomCharCol] = room[Direction.Forward] ? ROOM_UP_WALL_CHAR: EMPTY_CHAR;

            // down wall
            outText[downCharRow][roomCharCol] = room[Direction.Backwards] ? ROOM_DOWN_WALL_CHAR: EMPTY_CHAR;

            // left wall
            outText[roomCharRow][leftCharCol] = room[Direction.Left] ? ROOM_LEFT_WALL_CHAR: EMPTY_CHAR;

            // right wall
            outText[roomCharRow][rightCharCol] = room[Direction.Right] ? ROOM_RIGHT_WALL_CHAR: EMPTY_CHAR;
        }

        #endregion


        #region Help class

        private class RoomPositionChar: IRoomPosition
        {
            public ushort Row { get; set; } = 0;
            public ushort Column { get; set; } = 0;
            public char Char { get; set; } = ' ';

            public ushort Level => 0;

            public RoomPositionChar(ushort row, ushort column, char roomChar)
            {
                this.Row = row;
                this.Column = column;
                Char = roomChar;
            }

            public RoomPositionChar(ushort row, ushort column): this(row, column, ' ') { }
            
            

            public override bool Equals(object obj)
            {
                var @char = obj as RoomPositionChar;
                return @char != null &&
                       Row == @char.Row &&
                       Column == @char.Column;
            }

            public override int GetHashCode()
            {
                var hashCode = 240067226;
                hashCode = hashCode * -1521134295 + Row.GetHashCode();
                hashCode = hashCode * -1521134295 + Column.GetHashCode();
                return hashCode;
            }

            public override string ToString()
            {
                return String.Format("RoomPositionChar: {0}x{1} -> [{2}]", Row, Column, Char);
            }

            public static bool operator ==(RoomPositionChar char1, RoomPositionChar char2)
            {
                return EqualityComparer<RoomPositionChar>.Default.Equals(char1, char2);
            }

            public static bool operator !=(RoomPositionChar char1, RoomPositionChar char2)
            {
                return !(char1 == char2);
            }
        }


        #endregion
    }
}
