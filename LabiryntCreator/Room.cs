﻿using LabiryntCreator.Enums;
using LabiryntCreator.Interfaces;
using System;
using System.Collections.Generic;

namespace LabiryntCreator
{
    public class Room : IRoom
    {
        private Dictionary<Direction, bool> walls = new Dictionary<Direction, bool>();

        public bool this[Direction wall]
        {
            get { return walls[wall]; }
            set { walls[wall] = value; }
        }

        public Room()
        {
            CreateEmptyWalls();
        }

        private void CreateEmptyWalls()
        {
            walls.Clear();
            foreach (Direction wallType in (Direction[])Enum.GetValues(typeof(Direction)))
            {
                walls.Add(wallType, false);
            }
        }




    }
}