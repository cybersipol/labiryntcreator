﻿using LabiryntCreator.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabiryntCreator
{
    public class RoomFactory : IRoomFactory
    {
        public IRoom CreateRoom()
        {
            return new Room();
        }
    }
}
