﻿using LabiryntCreator.Interfaces;
using LabiryntCreator.Enums;
using System;
using System.Collections.Generic;
using LabiryntCreator.Commons;

namespace LabiryntCreator
{
    public class WallsCreator : IWallsCreator
    {
        public WallsCreator(IMatrix<IRoom> roomsMatrix)
        {
            RoomsMatrix = roomsMatrix ?? throw new ArgumentNullException(nameof(roomsMatrix));
            rnd = new Random();
        }

        public IMatrix<IRoom> RoomsMatrix { get; private set; }

        private Random rnd = null;

        public void BuildWalls()
        {
            BulidAllWallsForAllRooms();
            DemolishWallsMustBeAtLeastOneExistInEachRoom();
        }


        #region Help function

        private void BulidAllWallsForAllRooms()
        {
            RoomsMatrix.Reset();
            do
            {
                IRoom room = RoomsMatrix.Current;
                foreach (Direction wallDir in Direction.Forward.GetArrayForAll())
                    room[wallDir] = true;

            }
            while (RoomsMatrix.MoveNext());
        }

        private void DemolishWallsMustBeAtLeastOneExistInEachRoom()
        {
            RoomsMatrix.Reset();
            do
            {
                DemolishWallsMustBeAtLeastInRoom(RoomsMatrix.Current, RoomsMatrix.CurrentRow, RoomsMatrix.CurrentColumn);
            }
            while (RoomsMatrix.MoveNext());
        }

        private void DemolishWallsMustBeAtLeastInRoom(IRoom room, ushort row, ushort col)
        {
            if (IsOneExitInRoom(room)) return;

            List<Direction> lstDirs = GetArrayDirectionForRoom(room, row, col);
            if (lstDirs.Count > 0)
            {
                if (lstDirs.Count > 1)
                {
                    Direction[] randomDirs = lstDirs.ToArray();
                    (new Random()).Shuffle(randomDirs);
                    Direction rndDir = randomDirs[0];
                    room[rndDir] = false;
                }
                else room[lstDirs[0]] = false;
            }
        }


        private void BuildWallsOverHouse()
        {
            ushort row, col;
            
            for(col=0;col<RoomsMatrix.CountColumns;col++)
            {
                row = 0;
                RoomsMatrix.GetElement(row, col)[Direction.Forward] = true;
                row = (ushort)(RoomsMatrix.CountRows - 1);
                RoomsMatrix.GetElement(row, col)[Direction.Backwards] = true;
            }

            for (row = 0; row < RoomsMatrix.CountRows; row++)
            {
                col = 0;
                RoomsMatrix.GetElement(row, col)[Direction.Left] = true;
                col = (ushort)(RoomsMatrix.CountColumns - 1);
                RoomsMatrix.GetElement(row, col)[Direction.Right] = true;
            }

        }

        private void BulidWallsInRoomsAtLeastOneExitInEachRoom()
        {
            //RoomsMatrix.GetElement(0, 0)[Enums.WallDirection.Right] = true;
            ushort r = 0, c = 0;
            Random random = new Random();

            for (r= 0; r < RoomsMatrix.CountRows; r++)
            {
                for (c=0;c<RoomsMatrix.CountColumns; c++)
                {
                    IRoom room = RoomsMatrix.GetElement(r, c);
                    foreach (Direction wallDir in Direction.Forward.GetArrayForAll())
                    {
                        if (room[wallDir]) continue;

                        room[wallDir] = (random.Next(1, 100) > 90 ? true : false);
                    }
                }
            }

            RoomsMatrix.GetElement(0, 0)[Direction.Backwards] = true;
            RoomsMatrix.GetElement(0, 0)[Direction.Right] = true;
        } 


        private List<Direction> GetArrayDirectionForRoom(IRoom room, ushort row, ushort col)
        {
            List<Direction> outDirs = new List<Direction>();
            foreach (Direction wallDir in Direction.Forward.GetArrayForAll())
            {
                if (!room[wallDir]) continue;
                if (IsOuterWalls(row, col, wallDir)) continue;
                outDirs.Add(wallDir);
            }

            return outDirs;
        }

        private bool IsOneExitInRoom(IRoom room)
        {
            foreach (Direction wallDir in Direction.Forward.GetArrayForAll())
                if (!room[wallDir]) return true;

            return false;
        }

        private int CountAllDirectionValues()
        {
            return Direction.Forward.CountAll();
        }

        private bool IsOuterWalls(ushort row, ushort col, Direction wallDir)
        {
            if (wallDir == Direction.Left && IsFirstColumn(col)) return true;
            if (wallDir == Direction.Right && IsLastColumn(col)) return true;
            if (wallDir == Direction.Forward && IsFirstRow(row)) return true;
            if (wallDir == Direction.Backwards && IsLastRow(row)) return true;

            return false;

        }

        private bool IsLastColumn(ushort col) => (col >= (RoomsMatrix.CountColumns - 1));

        private bool IsFirstColumn(ushort col) => (col == 0);

        private bool IsLastRow(ushort row) => (row >= (RoomsMatrix.CountRows - 1));

        private bool IsFirstRow(ushort row) => (row == 0);


        #endregion

    }
}