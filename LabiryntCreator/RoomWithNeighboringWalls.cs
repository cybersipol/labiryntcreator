﻿using LabiryntCreator.Enums;
using LabiryntCreator.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LabiryntCreator
{
    public class RoomWithNeighboringWalls: IRoom
    {
        private IMatrixWithNeighbors<IRoom> matrix = null;
        private IRoom room = null;

        public RoomWithNeighboringWalls(IRoom room, IMatrixWithNeighbors<IRoom> matrix)
        {
            this.room = room ?? throw new ArgumentNullException(nameof(room));
            this.matrix = matrix;
        }

        public bool this[Direction wall]
        {
            get => room[wall];
            set => SetNeighboringWalls(wall, value);
        }

        public override string ToString()
        {
            return String.Format("type: [{0}], Walls: {1}left, {2}right, {3}up, {4}down",
                                    this.GetType().ToString(),
                                    this[Direction.Left] ? "+" : "-",
                                    this[Direction.Right] ? "+" : "-",
                                    this[Direction.Forward] ? "+" : "-",
                                    this[Direction.Backwards] ? "+" : "-"
                                    );
        }



        #region Help functions

        private void SetNeighboringWalls(Direction wall, bool val)
        {
            room[wall] = val;
            if (matrix == null) return;
            Direction oppWall = DirectionOpposed.GetOpposed(wall);

            IRoom nRoom = matrix.GetElementFromDirection(this, wall);

            if (nRoom != null && nRoom[oppWall]!=val) nRoom[oppWall] = val;
        }

        #endregion
    }
}