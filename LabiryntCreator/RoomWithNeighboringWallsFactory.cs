﻿using LabiryntCreator.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabiryntCreator
{
    public class RoomWithNeighboringWallsFactory : IRoomFactory
    {
        public IMatrixWithNeighbors<IRoom> RoomsMatrix { get; set; } = null;
        public IRoomFactory InnerRoomFactory { get; private set; } = null;

        public RoomWithNeighboringWallsFactory(IRoomFactory roomFactory)
        {
            this.InnerRoomFactory = roomFactory ?? throw new ArgumentNullException(nameof(roomFactory));
        }

        public IRoom CreateRoom()
        {
            return new RoomWithNeighboringWalls(InnerRoomFactory.CreateRoom(), RoomsMatrix);
        }
    }
}
