﻿using NUnit.Framework;
using LabiryntCreator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LabiryntCreator.Interfaces;
using LabiryntCreator.Enums;

namespace LabiryntCreator.Unit.Tests
{
    [TestFixture()]
    public class RoomWithNeighboringWalls_Tests
    {
        IMatrixWithNeighbors<IRoom> matrix = null;
        RoomWithNeighboringWalls sut = null;
        ushort TestRow = 0;
        ushort TestColumn = 0;

        [SetUp]
        public void Init()
        {
            matrix = WallsCreator_Tests.CreateMatrix(4, 5);
            TestRow = 2;
            TestColumn = 1;
        }


        #region SetWall Tests

        [TestCase(Direction.Left)]
        [TestCase(Direction.Right)]
        [TestCase(Direction.Forward)]
        [TestCase(Direction.Backwards)]
        public void SetWall_byDefault_NeighboringWallsIsSetOk(Direction wallDir)
        {
            sut = CreateSUT(TestRow, TestColumn);
            IRoom roomFromDirection = matrix.GetElementFromDirection(sut, wallDir);

            sut[wallDir] = true;

            Assert.IsNotNull(sut);
            Assert.IsNotNull(roomFromDirection);
            Assert.IsInstanceOf<RoomWithNeighboringWalls>(sut);
            Assert.IsInstanceOf<RoomWithNeighboringWalls>(roomFromDirection);

            Assert.IsTrue(sut[wallDir]);
            Assert.IsTrue(roomFromDirection[DirectionOpposed.GetOpposed(wallDir)]);
        }


        [TestCase(0, 1, Direction.Forward)]
        [TestCase(3, 1, Direction.Backwards)]
        [TestCase(2, 0, Direction.Left)]
        [TestCase(2, 4, Direction.Right)]
        public void SetWall_LastWall_NullDirectionRoom(int row, int col, Direction wallDir)
        {
            sut = CreateSUT((ushort)row, (ushort)col);
            IRoom roomFromDirection = matrix.GetElementFromDirection(sut, wallDir);

            sut[wallDir] = true;

            Assert.IsNotNull(sut);
            Assert.IsNull(roomFromDirection);

            Assert.IsTrue(sut[wallDir]);
        }

        #endregion

        #region Demolish Wall

        [TestCase(Direction.Left)]
        [TestCase(Direction.Right)]
        [TestCase(Direction.Forward)]
        [TestCase(Direction.Backwards)]
        public void DemolishWall_byDefault_NeighboringWalls(Direction wallDir)
        {
            sut = CreateSUT(TestRow, TestColumn);
            IRoom roomFromDirection = matrix.GetElementFromDirection(sut, wallDir);

            sut[wallDir] = true;
            Direction oppDir = DirectionOpposed.GetOpposed(wallDir);


            Assert.IsNotNull(sut);
            Assert.IsNotNull(roomFromDirection);
            Assert.IsInstanceOf<RoomWithNeighboringWalls>(sut);
            Assert.IsInstanceOf<RoomWithNeighboringWalls>(roomFromDirection);

            Assert.IsTrue(sut[wallDir]);
            Assert.IsTrue(roomFromDirection[oppDir]);

            roomFromDirection[oppDir] = false;

            Assert.IsFalse(sut[wallDir]);
            Assert.IsFalse(roomFromDirection[oppDir]);
        }


        [TestCase(0, 1, Direction.Forward)]
        [TestCase(3, 1, Direction.Backwards)]
        [TestCase(2, 0, Direction.Left)]
        [TestCase(2, 4, Direction.Right)]
        public void DemolishWall_LastWall_NullDirectionRoom(int row, int col, Direction wallDir)
        {
            sut = CreateSUT((ushort)row, (ushort)col);
            IRoom roomFromDirection = matrix.GetElementFromDirection(sut, wallDir);

            sut[wallDir] = true;

            Assert.IsNotNull(sut);
            Assert.IsNull(roomFromDirection);

            Assert.IsTrue(sut[wallDir]);

            sut[wallDir] = false;

            Assert.IsFalse(sut[wallDir]);
        }


        #endregion

        #region Help function

        private RoomWithNeighboringWalls CreateSUT(ushort row, ushort col)
        {
            IRoom room = matrix.GetElement(row, col);
            if (room == null) throw new ArgumentNullException(String.Format("No room in {0}x{1}", row, col));
            if (room is RoomWithNeighboringWalls)
            {
                sut = (RoomWithNeighboringWalls)room;
                
                return sut;
            }

            throw new ArgumentException(String.Format("No RoomWithNeighboringWalls type at position {0}x{1}", row, col));
        }

        #endregion



    }
}