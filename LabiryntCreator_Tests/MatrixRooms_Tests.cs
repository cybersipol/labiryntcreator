﻿using LabiryntCreator.Enums;
using LabiryntCreator.Interfaces;
using NUnit.Framework;
using System;

namespace LabiryntCreator.Unit.Tests
{
    [TestFixture()]
    public class MatrixRooms_Tests
    {
        private MatrixRooms sut = null;

        #region CountElements Tests

        [Test()]
        public void CountElements_NoElements_CountZeroElements()
        {
            CreateSUT();

            Assert.AreEqual(0, sut.CountColumns);
            Assert.AreEqual(0, sut.CountRows);
            Assert.AreEqual(0, sut.Count);
        }

        [Test()]
        public void CountElements_OneElement_ReturnCountOne()
        {
            CreateSUT(1,1);

            sut.SetElement(0, 0, new Room());

            Assert.AreEqual(1, sut.CountColumns);
            Assert.AreEqual(1, sut.CountRows);

            Assert.AreEqual(1, sut.Count);

        }

        [Test()]
        public void CountElements_3x2Array_ReturnCount16Elements()
        {
            ushort cols = 3;
            ushort rows = 2;
            CreateSUT(rows, cols);

            ushort c, r;
            for (r = 0; r < rows; r++)
                for (c = 0; c < cols; c++)
                    sut.SetElement(r, c, new Room());
            

            Assert.AreEqual(cols, sut.CountColumns);
            Assert.AreEqual(rows, sut.CountRows);

            Assert.AreEqual(cols*rows, sut.Count);
        }

        [Test()]
        public void CountElements_2x3ArrayNullElements_ReturnZeroCount()
        {
            ushort cols = 2;
            ushort rows = 3;
            CreateSUT(rows, cols, false);

            Assert.AreEqual(cols, sut.CountColumns);
            Assert.AreEqual(rows, sut.CountRows);

            Assert.AreEqual(0, sut.Count);
        }

        #endregion

        #region GetElement Tests

        [Test()]
        public void GetElement_3x2Array_ReturnOkElement()
        {
            ushort cols = 3;
            ushort rows = 2;
            CreateSUT(rows, cols);

            ushort c, r;
            for (r = 0; r < rows; r++)
                for (c = 0; c < cols; c++)
                    sut.SetElement(r, c, new Room());

            object result = sut.GetElement(1, 2);

            Assert.IsInstanceOf<Room>(result);
        }

        [Test()]
        public void GetElement_GetFromRowOverSize_ThrowExcption()
        {
            ushort cols = 3;
            ushort rows = 2;
            CreateSUT(rows, cols);

            Assert.Throws<ArgumentOutOfRangeException>(() => { sut.GetElement(rows, 0); });
        }

        [Test()]
        public void GetElement_GetFromColumnOverSize_ThrowExcption()
        {
            ushort cols = 3;
            ushort rows = 2;
            CreateSUT(rows, cols);

            Assert.Throws<ArgumentOutOfRangeException>(() => { sut.GetElement(0, cols); });
        }


        [Test()]
        public void GetElementFromDirection_GetElementRight_ReturnElement()
        {

            CreateSUT(4, 5);

            IRoom room2x1 = sut.GetElement(2, 1);
            IRoom roomRight = sut.GetElementFromDirection(room2x1, Direction.Right);

            Assert.IsNotNull(roomRight);
            Assert.AreSame(sut.GetElement(2, 2), roomRight);
        }

        [Test()]
        public void GetElementFromDirection_GetElementLeft_ReturnElement()
        {
            CreateSUT(4, 5);

            IRoom room2x1 = sut.GetElement(2, 1);
            IRoom roomLeft = sut.GetElementFromDirection(room2x1, Direction.Left);

            Assert.IsNotNull(roomLeft);
            Assert.AreSame(sut.GetElement(2, 0), roomLeft);
        }

        [Test()]
        public void GetElementFromDirection_GetElementUp_ReturnElement()
        {
            CreateSUT(4, 5);

            IRoom room2x1 = sut.GetElement(2, 1);
            IRoom roomUp = sut.GetElementFromDirection(room2x1, Direction.Forward);

            Assert.IsNotNull(roomUp);
            Assert.AreSame(sut.GetElement(1, 1), roomUp);
        }

        [Test()]
        public void GetElementFromDirection_GetElementDown_ReturnElement()
        {
            CreateSUT(4, 5);

            IRoom room2x1 = sut.GetElement(2, 1);
            IRoom roomDown = sut.GetElementFromDirection(room2x1, Direction.Backwards);

            Assert.IsNotNull(roomDown);
            Assert.AreSame(sut.GetElement(3, 1), roomDown);
        }

        [Test()]
        public void GetElementFromDirection_GetElementLeftOverColumnZero_ReturnNull()
        {
            CreateSUT(4, 5);

            IRoom room2x1 = sut.GetElement(2, 0);
            IRoom roomFromDirection = sut.GetElementFromDirection(room2x1, Direction.Left);

            Assert.IsNull(roomFromDirection);
        }

        [Test()]
        public void GetElementFromDirection_GetElementRightOverLastColumn_ReturnNull()
        {
            CreateSUT(4, 5);

            IRoom room2x1 = sut.GetElement(2, 4);
            IRoom roomFromDirection = sut.GetElementFromDirection(room2x1, Direction.Right);

            Assert.IsNull(roomFromDirection);
        }

        [Test()]
        public void GetElementFromDirection_GetElementUpOverRowZero_ReturnNull()
        {
            CreateSUT(4, 5);

            IRoom room2x1 = sut.GetElement(0, 1);
            IRoom roomFromDirection = sut.GetElementFromDirection(room2x1, Direction.Forward);

            Assert.IsNull(roomFromDirection);
        }

        [Test()]
        public void GetElementFromDirection_GetElementDownOverLastRow_ReturnNull()
        {
            CreateSUT(4, 5);

            IRoom room2x1 = sut.GetElement(3, 4);
            IRoom roomFromDirection = sut.GetElementFromDirection(room2x1, Direction.Backwards);

            Assert.IsNull(roomFromDirection);
        }

        #endregion

        #region SetElement Tests

        [Test]
        public void SetElement_SetNotNullElement_ReturnSameElement()
        {
            ushort cols = 2;
            ushort rows = 3;
            CreateSUT(rows, cols);
            Room box = new Room();
            sut.SetElement(2, 1, box);

            Assert.AreSame(box, sut.GetElement(2, 1));
        }

        [Test]
        public void SetElement_SetNullElement_ReturnNullElement()
        {
            ushort cols = 2;
            ushort rows = 3;
            CreateSUT(rows, cols);

            ushort c, r;
            for (r = 0; r < rows; r++)
                for (c = 0; c < cols; c++)
                    sut.SetElement(r, c, new Room());

            sut.SetElement(2, 1, null);

            Assert.IsNull( sut.GetElement(2, 1) );
        }

        [Test]
        public void SetElement_OverRowsSize_ThrowExcpetion()
        {
            ushort cols = 2;
            ushort rows = 3;
            CreateSUT(rows, cols);

            ushort c, r;
            for (r = 0; r < rows; r++)
                for (c = 0; c < cols; c++)
                    sut.SetElement(r, c, new Room());

            Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                sut.SetElement(rows, 1, null);
            });


        }

        [Test]
        public void SetElement_OverColumnsSize_ThrowExcpetion()
        {
            ushort cols = 2;
            ushort rows = 3;
            CreateSUT(rows, cols);

            ushort c, r;
            for (r = 0; r < rows; r++)
                for (c = 0; c < cols; c++)
                    sut.SetElement(r, c, new Room());

            Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                sut.SetElement(2, cols, null);
            });
        }

        #endregion


        #region Other Tests

        [Test()]
        public void SearchElement_SearchElementFromOutsideMatrix_NullMatrixPosition()
        {
            ushort cols = 2;
            ushort rows = 3;
            CreateSUT(rows, cols);
            Room box = new Room();

            IMatrixPosition result = sut.SearchElement(box);

            Assert.IsNull(result);
        }

        #endregion


        #region Help function

        public MatrixRooms CreateSUT(ushort numRows = 0, ushort numColumns = 0, bool createRooms = true)
        {
            sut = new MatrixRooms(numRows, numColumns);
            if (createRooms)
            {
                sut.RoomFactory = new RoomFactory();
                sut.FillAllElements();
            }
            return sut;
        }

        #endregion
    }
}