﻿using NUnit.Framework;
using LabiryntCreator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LabiryntCreator.Interfaces;

namespace LabiryntCreator.Unit.Tests
{
    [TestFixture()]
    public class LabiryntTextPreview_Tests
    {
        private LabiryntTextPreview sut = null;
        private IMatrix<IRoom> matrix = null;
        private IWallsCreator wallsCreator = null;


        [Test()]
        public void TextPreview_Test()
        {
            matrix = WallsCreator_Tests.CreateMatrix(4, 5);

            wallsCreator = new WallsCreator(matrix);
            wallsCreator.BuildWalls();

            sut = new LabiryntTextPreview(matrix);

            string result = sut.TextPreview();

            Console.WriteLine("----------------------------------");
            Console.WriteLine(result);
            Console.WriteLine("----------------------------------");

            Assert.GreaterOrEqual(result.Length, matrix.Count * sut.COUNT_TEXT_CHAR_COLUMN_PER_ONEROOM * sut.COUNT_TEXT_LINES_PER_ONEROOM);
        }

        [Test()]
        public void SetCharAtPosition_SetCharTInMatrix_ReturnTextWithOneCharT()
        {
            matrix = WallsCreator_Tests.CreateMatrix(4, 5);

            wallsCreator = new WallsCreator(matrix);
            wallsCreator.BuildWalls();

            sut = new LabiryntTextPreview(matrix);
            sut.SetCharAtPosition(2, 2, 'T');

            string result = sut.TextPreview();

            Console.WriteLine("----------------------------------");
            Console.WriteLine(result);
            Console.WriteLine("----------------------------------");

            StringAssert.Contains("T", result);
        }

        [Test()]
        public void GetCharAtPosition_SetCharsInMatrix_ReturnOkCharThisSamePosition()
        {
            matrix = WallsCreator_Tests.CreateMatrix(4, 5);

            wallsCreator = new WallsCreator(matrix);
            wallsCreator.BuildWalls();

            sut = new LabiryntTextPreview(matrix);
            sut.SetCharAtPosition(2, 2, 'T');
            sut.SetCharAtPosition(2, 3, 'A');

            char result1 = sut.GetCharAtPosition(2, 2);
            char result2 = sut.GetCharAtPosition(2, 3);

            Assert.AreEqual('T', result1);
            Assert.AreEqual('A', result2);
        }

        [Test()]
        public void RemoveCharAtPosition_SetCharsInMatrixAndRemove_RoomCharAtRemovedPosition()
        {
            matrix = WallsCreator_Tests.CreateMatrix(4, 5);

            wallsCreator = new WallsCreator(matrix);
            wallsCreator.BuildWalls();

            sut = new LabiryntTextPreview(matrix);
            sut.SetCharAtPosition(2, 2, 'T');
            sut.SetCharAtPosition(2, 3, 'A');

            sut.RemoveCharAtPosition(2, 2);

            char result1 = sut.GetCharAtPosition(2, 2);
            char result2 = sut.GetCharAtPosition(2, 3);

            Assert.AreEqual(sut.ROOM_CHAR, result1);
            Assert.AreEqual('A', result2);

        }

        [Test()]
        public void ClearAllChars_SetCharsInMatrixAndClear_RoomCharAtAllPosition()
        {
            matrix = WallsCreator_Tests.CreateMatrix(4, 5);

            wallsCreator = new WallsCreator(matrix);
            wallsCreator.BuildWalls();

            sut = new LabiryntTextPreview(matrix);
            sut.SetCharAtPosition(2, 2, 'T');
            sut.SetCharAtPosition(2, 3, 'A');

            sut.ClearAllChars();

            char result1 = sut.GetCharAtPosition(2, 2);
            char result2 = sut.GetCharAtPosition(2, 3);

            Assert.AreEqual(sut.ROOM_CHAR, result1);
            Assert.AreEqual(sut.ROOM_CHAR, result2);

            ushort r, c;
            for (r=0;r<matrix.CountRows;r++)
                for (c = 0; c < matrix.CountColumns; c++)
                    Assert.AreEqual(sut.ROOM_CHAR, sut.GetCharAtPosition(r, c));

        }
    }
}