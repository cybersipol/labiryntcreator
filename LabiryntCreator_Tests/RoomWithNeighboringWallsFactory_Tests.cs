﻿using NUnit.Framework;
using LabiryntCreator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LabiryntCreator.Interfaces;

namespace LabiryntCreator.Unit.Tests
{
    [TestFixture()]
    public class RoomWithNeighboringWallsFactory_Tests
    {

        IRoomFactory roomFactory = null;
        RoomWithNeighboringWallsFactory sut = null;

        [SetUp]
        public void Init()
        {
            roomFactory = new RoomFactory();
        }


        [Test()]
        public void ctor()
        {
            sut = CreateSUT();

            Assert.IsNotNull(sut);
            Assert.IsInstanceOf<RoomWithNeighboringWallsFactory>(sut);
            Assert.IsInstanceOf<RoomFactory>(sut.InnerRoomFactory);
        }

        [Test()]
        public void CreateRoom_byDefault_ReturnOkType()
        {
            sut = CreateSUT();
            sut.RoomsMatrix = new MatrixRooms(4, 5, sut);

            IRoom room = sut.CreateRoom();
            Assert.IsNotNull(room);
            Assert.IsInstanceOf<RoomWithNeighboringWalls>(room);
        }

        #region Help Funtion

        private RoomWithNeighboringWallsFactory CreateSUT(IRoomFactory factory)
        {
            sut = new RoomWithNeighboringWallsFactory(factory);
            return sut;
        }

        private RoomWithNeighboringWallsFactory CreateSUT()
        {
            return CreateSUT(roomFactory);
        }

        #endregion
    }
}