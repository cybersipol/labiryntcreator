﻿using NUnit.Framework;
using LabiryntCreator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LabiryntCreator.Interfaces;
using LabiryntCreator.Enums;


namespace LabiryntCreator.Unit.Tests
{
    [TestFixture()]
    public class WallsCreator_Tests
    {
        private WallsCreator sut = null;
        private MatrixRooms matrix = null;

        [Test()]
        public void ctor()
        {
            matrix = (MatrixRooms)CreateMatrix(4, 5);
            sut = new WallsCreator(matrix);

            Assert.IsNotNull(sut);
            Assert.AreSame(matrix, sut.RoomsMatrix);
        }

        [Test()]
        public void BuildWalls_CheckIsWallsOverHouse_AllWallsOverHouseAreExists()
        {
            ushort numRows = 4, numCols = 5, r=0, c=0;

            matrix = (MatrixRooms)CreateMatrix(numRows, numCols);
            sut = new WallsCreator(matrix);

            sut.BuildWalls();

            Assert.AreEqual(sut.RoomsMatrix.Count, numCols * numRows);

            Console.WriteLine(new LabiryntTextPreview(matrix).ToString());

            Assert_CheckIsWallsOverHouse(sut.RoomsMatrix);
        }

        public static void Assert_CheckIsWallsOverHouse(IMatrix<IRoom> matrix)
        {
            ushort numRows = matrix.CountRows;
            ushort numCols = matrix.CountColumns;
            ushort r, c;

            for (r = 0; r < numRows; r++)
            {
                c = 0;
                Assert.IsTrue(matrix.GetElement(r, c)[Direction.Left], String.Format("Room {0}x{1} no have outer wall", r, c));
                c = (ushort)(numCols - 1);
                Assert.IsTrue(matrix.GetElement(r, c)[Direction.Right], String.Format("Room {0}x{1} no have outer wall", r, c));
            }

            for (c = 0; c < numCols; c++)
            {
                r = 0;
                Assert.IsTrue(matrix.GetElement(r, c)[Direction.Forward], String.Format("Room {0}x{1} no have outer wall", r, c));
                r = (ushort)(numRows - 1);
                Assert.IsTrue(matrix.GetElement(r, c)[Direction.Backwards], String.Format("Room {0}x{1} no have outer wall", r, c));
            }

        }


        [Test()]
        public void BuildWalls_CheckIsInnerWallsHouse_OneOrMoreInnerWallExist()
        {
            ushort numRows = 4, numCols = 5, r = 0, c = 0;
            ushort firstRow = 0, lastRow = (ushort)(numRows - 1);
            ushort firstCol = 0, lastCol = (ushort)(numCols - 1);

            matrix = (MatrixRooms)CreateMatrix(numRows, numCols);
            sut = new WallsCreator(matrix);

            sut.BuildWalls();

            Assert.AreEqual(sut.RoomsMatrix.Count, numCols * numRows);

            int innerWallsCount = 0;

            for (r = 0; r < numRows; r++)
            {
                for (c = 0; c < numCols; c++)
                {
                    Interfaces.IRoom room = sut.RoomsMatrix.GetElement(r, c);
                    Assert.IsNotNull(room, String.Format("Room {0}x{1} is null", r, c));

                    if (c != firstCol && room[Direction.Left]) innerWallsCount++;
                    if (c != lastCol && room[Direction.Right]) innerWallsCount++;

                    if (r != firstRow && room[Direction.Forward]) innerWallsCount++;
                    if (r != lastRow && room[Direction.Backwards]) innerWallsCount++;

                }
            }

            Assert.Greater(innerWallsCount, 0);
            Assert_CheckIsWallsOverHouse(sut.RoomsMatrix);
        }

        [Test()]
        public void BuildWalls_CheckIsNeighboringWalls_NeighboringWallsAreSet()
        {
            ushort numRows = 4, numCols = 5, r = 0, c = 0;
            ushort firstRow = 0, lastRow = (ushort)(numRows - 1);
            ushort firstCol = 0, lastCol = (ushort)(numCols - 1);

            matrix = (MatrixRooms)CreateMatrix(numRows, numCols);
            sut = new WallsCreator(matrix);

            sut.BuildWalls();

            Assert.AreEqual(sut.RoomsMatrix.Count, numCols * numRows);
            Console.WriteLine(new LabiryntTextPreview(matrix).ToString());

            for (r = 0; r < numRows; r++)
            {
                for (c = 0; c < numCols; c++)
                {
                    IRoom room = sut.RoomsMatrix.GetElement(r, c);
                    Assert.IsNotNull(room, String.Format("Room {0}x{1} is null", r, c));

                    if (c != firstCol && room[Direction.Left]) Assert.IsTrue(sut.RoomsMatrix.GetElement(r, (ushort)(c - 1))[Direction.Right], String.Format("Neighboring Walls in room {0}x{1} not set (Left-Right)", r, c));
                    if (c != lastCol && room[Direction.Right]) Assert.IsTrue(sut.RoomsMatrix.GetElement(r, (ushort)(c + 1))[Direction.Left], String.Format("Neighboring Walls in room {0}x{1} not set (Right-Left)", r, c));

                    if (r != firstRow && room[Direction.Forward]) Assert.IsTrue(sut.RoomsMatrix.GetElement((ushort)(r - 1), c)[Direction.Backwards], String.Format("Neighboring Walls in room {0}x{1} not set (Up-Down)", r, c));
                    if (r != lastRow && room[Direction.Backwards]) Assert.IsTrue(sut.RoomsMatrix.GetElement((ushort)(r + 1), c)[Direction.Forward], String.Format("Neighboring Walls in room {0}x{1} not set (Down-Up)", r, c));
                }
            }
            Assert_CheckIsWallsOverHouse(sut.RoomsMatrix);
        }


        [Test()]
        public void BuildWalls_CheckIfExistsRoomWithoutExist_NoExists()
        {
            ushort numRows = 4, numCols = 5, r = 0, c = 0;

            matrix = (MatrixRooms) CreateMatrix(numRows, numCols);
            sut = new WallsCreator(matrix);

            sut.BuildWalls();

            Assert.AreEqual(sut.RoomsMatrix.Count, numCols * numRows);
            Console.WriteLine(new LabiryntTextPreview(matrix).ToString());

            matrix.Reset();
            do
            {
                IRoom room = matrix.Current;
                int ileWalls = 0;
                int numWall = ((Direction[])Enum.GetValues(typeof(Direction))).Length;
                foreach (Direction wallDir in (Direction[])Enum.GetValues(typeof(Direction)))
                {
                    if (room[wallDir]) ileWalls++;
                }

                Assert.Less(ileWalls, numWall, String.Format("Room {0}x{1} no have exit", matrix.CurrentRow, matrix.CurrentColumn ));
            }
            while (matrix.MoveNext());

            Assert_CheckIsWallsOverHouse(sut.RoomsMatrix);
        }


        #region Help function

        internal static IMatrixWithNeighbors<IRoom> CreateMatrix(ushort numRows, ushort numCols)
        {
            MatrixRooms mRooms = new MatrixRooms(numRows, numCols);
            RoomWithNeighboringWallsFactory factory = new RoomWithNeighboringWallsFactory(new RoomFactory());
            factory.RoomsMatrix = mRooms;
            mRooms.RoomFactory = factory;
            mRooms.FillAllElements();

            return  mRooms;
        }


        #endregion
    }
}