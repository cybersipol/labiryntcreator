﻿using NUnit.Framework;
using LabiryntCreator.Enums;
using System;

namespace LabiryntCreator.Enums.Unit.Tests
{
    [TestFixture()]
    public class DirectionOpposed_Tests
    {
        [Test()]
        public void GetOpposed_CheckAllDirection_EachDirectionHasOpposedDirection()
        {
            foreach (Direction wallDir in (Direction[])Enum.GetValues(typeof(Direction)))
            {
                object result = DirectionOpposed.GetOpposed(wallDir);
                Assert.IsNotNull(result, "No opposed direction for {0}", wallDir.ToString());
                Assert.IsInstanceOf<Direction>(result, "direction wrong type");
            }

        }
    }
}