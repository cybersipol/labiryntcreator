﻿using NUnit.Framework;
using LabiryntCreator.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabiryntCreator.Enums.Unit.Tests
{
    [TestFixture()]
    public class DirectionDelta_Tests
    {
        [Test()]
        public void DirectionDelta_CheckAllDirectionHaveDelta_AllDirectionHasDelta()
        {
            foreach (Direction wallDir in (Direction[])Enum.GetValues(typeof(Direction)))
            {
                object result = DirectionDelta.GetDelta(wallDir);
                Assert.IsNotNull(result, "No direction delta for {0}, check class DirectionDelta", wallDir.ToString());
                Assert.IsInstanceOf<DirectionDelta>(result, "direction delta wrong type");
            }


        }
    }
}